import axios from 'axios'
import * as dotenv from "dotenv";

dotenv.config({ path: __dirname + '/.env' });

const instance = axios.create({
    baseURL: process.env.REACT_APP_BACKEND,
    headers: {
        "Content-Type": "application/json"
    }
});

const instanceGCP = axios.create({
    baseURL: process.env.REACT_APP_GCP_LINK,
    headers: {
        "Content-Type": "application/json"
    }
});

// An interceptor is more of a checkpoint for every HTTP action and it is attached to an Axios instance.
// Every API call that has been made, is passed through this interceptor.
instance.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");

    if (token && config.headers) {
        config.headers.Authorization = 'Bearer ' + token;
    }

    return config;
}, (error) => {
    if (error.response && error.response.data) {
        return Promise.reject(error.response.data);
    }
    return Promise.reject(error.message);
});

export { instance, instanceGCP };