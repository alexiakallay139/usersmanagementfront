import Login from './components/Login';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Datepicker from './components/Datepicker';
import ResultsTable from './components/Table';
import Upload from './components/UploadImage';
import useAuth from './hooks/useAuth';
import { AppBar, Toolbar, Typography, Button, Link, Alert, Snackbar } from '@mui/material';
import { Box } from '@mui/system';
import { useState } from 'react';

function App() {
  const { token, logout } = useAuth();
  const [showSnackBar, setShowSnackBar] = useState(false);
  const [message, setMessage] = useState("");
  const pages = [
    {
      name: "Upload Image",
      link: "/upload_file"
    },
    {
      name: "Image Recognition Results",
      link: "/view_table"
    },
    {
      name: "Schedules",
      link: "/view_calendar"
    }];

  const RestrictedRoute = ({ ...props }) => {
    if (token) {
      return <Route {...props} />;
    }
    return <Redirect to="/" />;
  }

  const handleLogout = () => {
    logout();
    localStorage.removeItem("imageResults");
    setMessage("You have been logged out.");
    setShowSnackBar(true);
  }

  return (
    <>
      <Box sx={{ flexGrow: 1, mb: 0 }} >
        <AppBar position="sticky" sx={{ backgroundColor: "#785ae6" }}>
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Image Recognition App
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              {
                pages.map((page) => (
                  <Button
                    key={page.name}>
                    <Link href={page.link} sx={{ my: 2, color: '#fff', display: 'block' }}>{page.name}</Link>
                  </Button>
                ))
              }
            </Box>
            <Button color="inherit" onClick={handleLogout}>Log out</Button>
          </Toolbar>
        </AppBar>
      </Box>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} />
          <RestrictedRoute exact path="/view_calendar" component={Datepicker} />
          <RestrictedRoute exact path="/view_table" component={ResultsTable} />
          <RestrictedRoute exact path="/upload_file" component={Upload} />
        </Switch>
      </BrowserRouter>
      {
        showSnackBar ?
          <Snackbar open={showSnackBar} autoHideDuration={5000} onClose={() => { setShowSnackBar(false); }}>
            <Alert severity="info">{message}</Alert>
          </Snackbar> : null
      }
    </>
  );
}

export default App;
