import { SetStateAction, useState } from 'react';
import { Button, Divider, MenuItem, Paper, Select, Stack, styled, Box } from '@mui/material';

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    width: 450,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: '#fff',
    background: 'transparent'
}));

function Calendar(props: any) {

    const [monthState, setMonth] = useState({
        month: '',
        showCalendarForJan: false,
        showCalendarForFeb: false,
        showCalendarForMar: false,
        showCalendarForApr: false,
        showCalendarForMay: false,
        showCalendarForJune: false,
        showCalendarForJuly: false,
        showCalendarForAug: false,
        showCalendarForSept: false,
        showCalendarForOct: false,
        showCalendarForNov: false,
        showCalendarForDec: false
    });

    const getInitialStateWithMonth = (monthValue: any) => ({
        month: monthValue,
        showCalendarForJan: false,
        showCalendarForFeb: false,
        showCalendarForMar: false,
        showCalendarForApr: false,
        showCalendarForMay: false,
        showCalendarForJune: false,
        showCalendarForJuly: false,
        showCalendarForAug: false,
        showCalendarForSept: false,
        showCalendarForOct: false,
        showCalendarForNov: false,
        showCalendarForDec: false
    });

    const [dayState, setDay] = useState('');
    const [intervalState, setInterval] = useState({
        active: false,
        startDayInterval: '',
        endDayInterval: '',
        startMonth: '',
        endMonth: ''
    });

    const handleChangeForMonth = (e: { target: { value: any; }; }) => {
        setInterval({ ...intervalState, startDayInterval: dayState, startMonth: e.target.value });
        switch (e.target.value) {
            case "01":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForJan: true });
                break;
            case "02":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForFeb: true });
                break;
            case "03":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForMar: true });
                break;
            case "04":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForApr: true });
                break;
            case "05":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForMay: true });
                break;
            case "06":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForJune: true });
                break;
            case "07":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForJuly: true });
                break;
            case "08":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForAug: true });
                break;
            case "09":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForSept: true });
                break;
            case "10":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForOct: true });
                break;
            case "11":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForNov: true });
                break;
            case "12":
                setMonth({ ...getInitialStateWithMonth(e.target.value), showCalendarForDec: true });
                break;
        }
    };

    const handleInterval = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.currentTarget.style.backgroundColor = "rgb(208, 76, 208)";
        if (intervalState.startDayInterval === '') {
            setInterval({ active: true, startDayInterval: (e.target as HTMLButtonElement).value, endDayInterval: '', startMonth: monthState.month, endMonth: '' });
        }
        else {
            setInterval({ ...intervalState, endDayInterval: (e.target as HTMLButtonElement).value, endMonth: monthState.month });
        }
    }

    const handleChangeForDay = (e: { target: { value: SetStateAction<string>; }; }) => {
        setDay(e.target.value);
        setInterval({ ...intervalState, startDayInterval: e.target.value.toString(), startMonth: monthState.month });
    };

    const removeButtonsBackground = () => {
        const buttons = document.querySelectorAll("button");
        if (buttons.length > 0) {
            for (var index = 0; index < buttons.length; ++index) {
                if (buttons[index].style.backgroundColor === "rgb(208, 76, 208)") {
                    buttons[index].style.backgroundColor = "transparent";
                }
            }
        }
    };

    return (
        <div className='Calendar' style={{ background: 'linear-gradient(to bottom right,rgb(128, 0, 128),#a55bcf,rgb(65, 65, 255), #4994ce,rgb(65, 65, 255), #a55bcf, rgb(202, 0, 202)' }}>
            <Stack
                direction="row"
                width={450}
                divider={<Divider orientation="vertical" flexItem sx={{ background: '#fff' }} />}
            >
                <Item elevation={0}>
                    <Select value={dayState} onChange={handleChangeForDay} label="Day" sx={{ color: '#fff', height: 30 }}>
                        {Array.from(Array(31).keys()).map((value, index) => (
                            <MenuItem value={value + 1} defaultValue={1}>{value + 1}</MenuItem>
                        ))}
                    </Select>
                </Item>

                <Item elevation={0}>
                    <Select value={monthState.month} onChange={handleChangeForMonth} label="Month" sx={{ color: '#fff', height: 30 }}>
                        {["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"].map((value, index) => (
                            <MenuItem value={value} defaultValue={"01"}>{value}</MenuItem>
                        ))}
                    </Select>
                </Item>
                <Item elevation={0}>
                    <Select value={'2021'} label="Year" sx={{ color: '#fff', height: 30 }}>
                        <MenuItem value={'2021'}>2021</MenuItem>
                    </Select></Item>
            </Stack>

            <Stack
                direction="row"
                width={450}
            >
                {["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"].map((value, index) => (
                    <Item elevation={0}>{value}</Item>
                ))}
            </Stack>

            {
                monthState.showCalendarForJan ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>27</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>28</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>29</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(30).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForFeb ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(28).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>2</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>3</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>4</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>5</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>6</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForMar ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>28</Button>
                            {Array.from(Array(31).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>2</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>3</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForApr ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>28</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>29</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(30).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForMay ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>26</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>27</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>28</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>29</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(31).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>2</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>3</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>4</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>5</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForJune ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(30).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>2</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>3</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForJuly ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>28</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>29</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(31).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForAug ?
                    <>
                        <Box className="days" width={450}>
                            {Array.from(Array(31).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>2</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>3</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>4</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForSept ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>29</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(30).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>2</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForOct ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>27</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>28</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>29</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(30).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForNov ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>28</Button>
                            {Array.from(Array(30).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>2</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>3</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>4</Button>
                        </Box>
                    </> : null
            }
            {
                monthState.showCalendarForDec ?
                    <>
                        <Box className="days" width={450}>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>29</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>30</Button>
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>31</Button>
                            {Array.from(Array(31).keys()).map((value, index) => (
                                <Button
                                    key={index}
                                    sx={{ color: '#fff' }}
                                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                                        handleInterval(e);
                                    }}
                                    value={value + 1}>
                                    {value + 1}
                                </Button>
                            ))}
                            <Button sx={{ color: 'rgba(255,255,255,0.3)' }}>1</Button>
                        </Box>
                    </> : null
            }
            <div className='grid-item buttons' style={{
                background: 'linear-gradient(to bottom right, #d04cd0, #4994ce, #a55bcf)',
                display: "flex",
                justifyContent: "center"
            }}>
                <Button variant="contained"
                    sx={{
                        bgcolor: '#fff',
                        color: 'rgb(131, 128, 128)',
                        borderRadius: 10,
                        width: 90,
                        height: 40,
                        margin: 1,
                        marginLeft: 4,
                    }}
                    onClick={() => {
                        setInterval({ active: false, startDayInterval: '', endDayInterval: '', startMonth: '', endMonth: '' });
                        removeButtonsBackground();
                    }}>
                    Cancel
                </Button>
                <Button variant="contained"
                    sx={{
                        margin: 1,
                        marginRight: 4,
                        bgcolor: '#d04cd0',
                        color: '#fff',
                        borderRadius: 10,
                        width: 90,
                        height: 40
                    }}
                    onClick={() => {
                        props.modifyInterval(intervalState.startDayInterval + "/" + intervalState.startMonth + "/2021", intervalState.endDayInterval + "/" + intervalState.endMonth + "/2021");
                        setInterval({
                            active: false,
                            startDayInterval: '',
                            endDayInterval: '',
                            startMonth: '',
                            endMonth: ''
                        });

                        removeButtonsBackground();
                    }}>
                    Done
                </Button>
            </div>
        </div >
    )
}

export default Calendar;