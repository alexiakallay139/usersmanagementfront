import '../Calendar.scss'
import { useEffect, useState } from 'react';
import Calendar from './Calendar'
import { Alert, Divider, Snackbar, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { instanceGCP } from '../api/instance';
import { Box } from '@mui/system';

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    width: 450,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: '#fff',
    background: 'transparent'
}));

interface ISchedule {
    start_date: String,
    end_date: String
}

function Datepicker() {
    const [showCalendar, setShowCalendar] = useState(false);
    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [showSnackBar, setShowSnackBar] = useState(false);
    const [message, setMessage] = useState("");
    const [schedules, setSchedules] = useState<ISchedule[]>([]);

    const showCalendarOnClick = () => {
        setShowCalendar(!showCalendar);
    }

    useEffect(() => {
        (async function () {
            await instanceGCP.get('/getSchedules')
                .then((response: any) => {
                    setSchedules((old: any) => [...old, ...response.data]);
                }).catch(error => {
                    console.log(error);
                });
        })();
    }, []);

    const removeFaultySchedule = (schedule: ISchedule) => {
        const filtered = schedules.filter(item => item !== schedule);
        setSchedules(filtered);
        setStartDate("");
        setEndDate("");
    }

    const handleSnackBar = (message: string) => {
        setMessage(message);
        setShowSnackBar(true);
    }

    const handleInterval = async (startDate: string, endDate: string) => {
        setStartDate(startDate);
        setEndDate(endDate);

        const newSchedule = {
            start_date: startDate,
            end_date: endDate
        };

        // add new entity before OK response
        setSchedules((old: any) => [...old, newSchedule]);

        await instanceGCP.post('/createSchedule', {
            "start_date": startDate,
            "end_date": endDate
        }).then(response => {
            if (response.status !== 200) {
                // if status is not OK, remove last item from table
                handleSnackBar(JSON.stringify(response.data) + " Please pick a new interval.");
                removeFaultySchedule(newSchedule);
            } else {
                handleSnackBar("The new schedule has been added.");
                setStartDate("");
                setEndDate("");
            }
        }).catch(error => {
            // if an error has occurred, remove last item from table
            handleSnackBar(error.response.data + " Please pick a new interval.");
            removeFaultySchedule(newSchedule);
        });
    }

    return (
        <>
            <Stack direction="row" spacing={2} mt={2}>
                <Item>
                    <div style={{
                        marginTop: "10px",
                        display: "grid",
                        margin: "auto",
                        maxWidth: "450px",
                        background: 'linear-gradient(to bottom right,rgb(128, 0, 128),#a55bcf,rgb(65, 65, 255),#4994ce,rgb(65, 65, 255),#a55bcf,rgb(202, 0, 202))'
                    }}>
                        <Stack
                            direction="row"
                            width={450}
                            divider={<Divider orientation="vertical" flexItem sx={{ background: '#fff' }} />}
                            onClick={showCalendarOnClick}
                        >
                            <Item elevation={0}>
                                Start Date <br />
                                {startDate}
                            </Item>
                            <Item elevation={0}>
                                End Date <br />
                                {endDate}
                            </Item>
                        </Stack>
                        {showCalendar ?
                            <Calendar modifyInterval={handleInterval} /> : null}
                    </div >
                </Item>
                <Item sx={{ width: "inherit", height: "100%" }}>
                    <TableContainer sx={{ width: 700, height: 400, background: "rgba(192,192,192,0.3)", m: "auto", p: 3, pl: 25 }}>
                        <Table sx={{ background: "#fff", width: 600, height: 400 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>
                                        <Box sx={{ fontWeight: "bold" }}>Start Date</Box>
                                    </TableCell>
                                    <TableCell align="right">
                                        <Box sx={{ fontWeight: "bold" }}>End Date
                                        </Box>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {schedules !== null ? schedules.map((row: ISchedule, index) => (
                                    <TableRow
                                        key={index}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.start_date}
                                        </TableCell>
                                        <TableCell align="right">{row.end_date}</TableCell>
                                    </TableRow>
                                )) : <TableRow>Loading...</TableRow>}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Item>
            </Stack>
            {
                showSnackBar ?
                    <Snackbar open={showSnackBar} autoHideDuration={5000} onClose={() => { setShowSnackBar(false); }}>
                        <Alert severity="info">{message}</Alert>
                    </Snackbar> : null
            }
        </>
    )
}

export default Datepicker;