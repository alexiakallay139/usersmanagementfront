import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import { instance } from "../api/instance";
import { useEffect, useState } from 'react';
import { Snackbar, Alert } from '@mui/material';

export default function Upload() {
    const [, setPicture] = useState<any | null>(null);
    const [imgData, setImgData] = useState<any | null>(null);
    const [showSnackBar, setShowSnackBar] = useState(false);
    const [message, setMessage] = useState("");
    const [storedRecognitionResult, setStoredResult] = useState<any[]>([]);

    useEffect(() => {
        localStorage.setItem("imageResults", JSON.stringify(storedRecognitionResult));
    }, [storedRecognitionResult]);

    const sendImageToEvaluate = (e: React.ChangeEvent<any>) => {
        let file = e.target.files[0];

        if (file) {
            if (!file.name.match(/.(png)$/i)) {
                setShowSnackBar(true);
                setMessage("Sorry, we only accept PNG files.");
            } else {
                var image = new Image();
                image.src = window.URL.createObjectURL(file);
                image.onload = () => {
                    if (image.width === 28 && image.height === 28) {
                        setPicture(e.target.files[0]);

                        // display image in browser
                        const reader = new FileReader();
                        reader.addEventListener("load", () => {
                            setImgData(reader.result);
                        });
                        reader.readAsDataURL(e.target.files[0]);

                        // send to evaluate
                        let data = new FormData();
                        data.append("image", file);
                        instance
                            .post("/evaluate", data, {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then(response => {
                                setShowSnackBar(true);
                                setMessage("Recognition results are available at /view_table endpoint. Refresh on that page is necessary.");
                                setStoredResult((old: any) => [...old,
                                {
                                    "name": file.name,
                                    "size": `${image.width} x ${image.height}`,
                                    "recognitionResult": response.data[0],
                                    "downloadLink": file.name
                                }
                                ]);
                            }).catch(error => {
                                console.log(error);
                            });
                    } else {
                        setShowSnackBar(true);
                        setMessage(`Sorry, this image doesn't look like the size we wanted. It's ${image.width} x ${image.height} but we require 28 x 28 size image.`);
                    }
                }
            }
        }
    }

    return (
        <>
            <Stack
                direction="column">
                <Box sx={{
                    width: 500,
                    height: 400,
                    m: "auto",
                    mt: 10,
                    p: 5,
                    backgroundColor: 'lightgrey',
                    borderColor: 'text.primary',
                }}
                    textAlign="right"
                ><Button variant="contained" component="label"
                    sx={{
                        bgcolor: 'gray',
                        color: 'black'
                    }}>
                        Upload
                        <input type="file" name="image" hidden onChange={sendImageToEvaluate} />
                    </Button>
                    <Box sx={{
                        width: 450,
                        height: 300,
                        m: 2,
                        p: 2,
                        backgroundColor: 'gray',
                        borderColor: 'text.primary'
                    }}
                        textAlign="center"
                        alignItems="center"
                        justifyContent="center"
                        display="flex"
                    >
                        <img className="uploaded_image" src={imgData} alt="Waiting for a file..." />
                    </Box>
                </Box>
            </Stack>
            {
                showSnackBar ?
                    <Snackbar open={showSnackBar} autoHideDuration={5000} onClose={() => { setShowSnackBar(false); }}>
                        <Alert severity="info">{message}</Alert>
                    </Snackbar> : null
            }
        </>
    );
}
