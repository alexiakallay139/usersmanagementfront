import Table from '@mui/material/TableBody';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Box } from '@mui/system';

interface IRow {
  name: string,
  size: string,
  recognitionResult: string,
  downloadLink: string
}

export default function ResultsTable() {
  const storedRecognitionResult = localStorage.getItem("imageResults");

  return (
    <TableContainer sx={{ width: 700, height: 400, background: "lightgrey", m: "auto", mt: 10, p: 5, pl: 25 }}>
      <Table sx={{ background: "#fff", width: 600, height: 400 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell><Box sx={{ fontWeight: "bold" }}>Image name</Box></TableCell>
            <TableCell align="right"><Box sx={{ fontWeight: "bold" }}>Size</Box></TableCell>
            <TableCell align="right"><Box sx={{ fontWeight: "bold" }}>Recognition result</Box></TableCell>
            <TableCell align="right"><Box sx={{ fontWeight: "bold" }}>Image download link</Box></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {storedRecognitionResult !== null ? JSON.parse(storedRecognitionResult).map((row: IRow) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.size}</TableCell>
              <TableCell align="right">{row.recognitionResult}</TableCell>
              <TableCell align="right">
                <a href={row.downloadLink} download>Download</a>
              </TableCell>
            </TableRow>
          )) : <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
            <TableCell component="th" scope="row">
              No data
            </TableCell>
          </TableRow>}
        </TableBody>
      </Table>
    </TableContainer>
  );
}