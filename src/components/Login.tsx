import { Container, Avatar, Button, Grid, Link, Typography, TextField, Stack, Alert, Snackbar } from "@mui/material";
import { Box } from "@mui/system";
import EmailIcon from '@mui/icons-material/Email';
import { InputAdornment } from "@mui/material";
import LockIcon from '@mui/icons-material/Lock';
import AvatarImage from '../../src/images/login_avatar.png'
import useAuth from "../hooks/useAuth";
import { useState } from "react";
import PropTypes from 'prop-types';

export default function Login() {
    const { login } = useAuth();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [showSnackBar, setShowSnackBar] = useState(false);
    const [message, setMessage] = useState("");

    const onChangeEmail = (e: React.ChangeEvent<any>) => {
        e.preventDefault();
        setEmail(e.target.value);
    };

    const onChangePassword = (e: React.ChangeEvent<any>) => {
        e.preventDefault();
        setPassword(e.target.value);
    };


    const handleSubmit = async () => {
        if (email === "" || password === "") {
            setShowSnackBar(true);
            setMessage("All fields are required.");
            return;
        }
        await login(email, password, function (status: any) {
            setShowSnackBar(true);
            if (status === 200) {
                setMessage("Successful login! A token has been generated.");
            } else if (status === 400) {
                setMessage("Invalid credentials.");
            }
        });
    };

    return (
        <Box sx={{
            background: "linear-gradient(to right top, blue, pink)",
            height: "88vh",
            width: "100wh",
            overflowY: "hidden"
        }}>
            <Container sx={{ m: 15, p: 10, border: 1, borderRadius: 5, background: '#fff' }}>
                <Grid container>
                    <Grid item xs={6}>
                        <Avatar src={AvatarImage} sx={{ width: 300, height: 300, ml: 10 }}>
                        </Avatar>
                    </Grid>
                    <Grid item xs={6}>
                        <Stack
                            direction="column"
                            width={450}
                            sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
                        >
                            <Typography sx={{ fontSize: 28, mb: 2 }}> User Login
                            </Typography>
                            <TextField
                                type="email"
                                margin="normal"
                                name="email"
                                placeholder="Email Id"
                                onChange={onChangeEmail}
                                style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <EmailIcon />
                                        </InputAdornment>
                                    )
                                }}
                            />
                            <TextField
                                type="password"
                                margin="normal"
                                name="password"
                                placeholder="Password"
                                onChange={onChangePassword}
                                style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockIcon />
                                        </InputAdornment>
                                    )
                                }} />
                            <Button
                                type="submit"
                                variant="contained"
                                onClick={handleSubmit}
                                sx={{ mt: 3, mb: 2, width: 260, borderRadius: 10 }}
                            >
                                Login
                            </Button>
                            <Link href="#" variant="body2">
                                Forgot Username / Password?
                            </Link>
                        </Stack>
                    </Grid>
                </Grid>
                {
                    showSnackBar ?
                        <Snackbar open={showSnackBar} autoHideDuration={5000} onClose={() => { setShowSnackBar(false); }}>
                            <Alert severity="info">{message}</Alert>
                        </Snackbar> : null
                }
            </Container>
        </Box>
    )
};

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}
