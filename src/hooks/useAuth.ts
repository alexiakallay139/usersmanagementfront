import { instance } from "../api/instance"
import useTokenStorage from "./useToken";

export default function useAuth() {
    const [token, setToken] = useTokenStorage();

    const login = async (email: string, password: string, callback: any) => {
        instance
            .post("/login", {
                "email": email,
                "password": password
            })
            .then(response => {
                callback(response.status);
                if (response.status === 200) {
                    setToken(response.data.token);
                }
                return response.status;
            }).catch(error => {
                callback(error.response.status);
            });
    };

    const logout = () => {
        setToken("");
    };

    return { token, login, logout } as const;
};
