import { useEffect, useState } from 'react';

const useTokenStorage = () => {
    const [token, setToken] = useState(() => {
        const token = localStorage.getItem("token");
        return token !== null ? token : ""
    });

    useEffect(() => {
        if (token === "") {
            localStorage.removeItem("token");
        } else {
            localStorage.setItem("token", token);
        }
    }, [token]);

    return [token, setToken] as const;
};

export default useTokenStorage;